import React, {Component} from "react";
import {View, Text, StyleSheet} from "react-native";
import { Navigation } from 'react-native-navigation'

class merah extends Component {

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.title}>R</Text>
                <Text style={styles.text}>Ini adalah Red</Text>
            </View>
        );
    }
}
export default merah ;

const styles = StyleSheet.create({
    container:{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'red'
    },
    title:{
        fontSize: 70,
        color: 'white'
    },
    text:{
        fontSize: 18,
        color: 'black'
    },
});


