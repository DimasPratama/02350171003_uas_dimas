import React, {Component} from "react";
import {View, Text, StyleSheet} from "react-native";

class biru extends Component {

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.title}>B</Text>
                <Text style={styles.text}>Ini adalah Blue</Text>
            </View>
        );
    }
}
export default biru ;

const styles = StyleSheet.create({
    container:{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'blue'
    },
    title:{
        fontSize: 70,
        color: 'white'
    },
    text:{
        fontSize: 18,
        color: 'black'
    },
});


