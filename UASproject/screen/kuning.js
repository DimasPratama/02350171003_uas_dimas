import React, {Component} from "react";
import {View, Text, StyleSheet} from "react-native";

class kuning extends Component {

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.title}>Y</Text>
                <Text style={styles.text}>Ini adalah yellow</Text>
            </View>
        );
    }
}
export default kuning ;

const styles = StyleSheet.create({
    container:{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'yellow'
    },
    title:{
        fontSize: 70,
        color: 'white'
    },
    text:{
        fontSize: 18,
        color: 'black'
    },
});


