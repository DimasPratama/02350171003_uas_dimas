import React, {Component} from "react";
import {View, Text, StyleSheet} from "react-native";

class hijau extends Component {

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.title}>G</Text>
                <Text style={styles.text}>Ini adalah Green</Text>
            </View>
        );
    }
}
export default hijau ;

const styles = StyleSheet.create({
    container:{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'green'
    },
    title:{
        fontSize: 70,
        color: 'white'
    },
    text:{
        fontSize: 18,
        color: 'black'
    },
});


