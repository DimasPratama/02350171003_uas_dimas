import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
} from "react-native";
import { Navigation } from 'react-native-navigation'

class beranda extends Component {

    goToScreen = (screenName) => {
        Navigation.push(this.props.componentId, {
            component: {
                name : screenName
            }
            })
    }

    render(){
        return(
            <View style={styles.container}>
                <View style={styles.layer}>
                    <TouchableOpacity style={styles.merah} onPress={()=> this.goToScreen('merah')}>
                        <Text style={styles.text}>R</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.hijau} onPress={()=> this.goToScreen('hijau')}>
                        <Text style={styles.text}>G</Text>
                    </TouchableOpacity>
                </View>

                <View style={styles.layer}>
                    <TouchableOpacity style={styles.biru} onPress={()=> this.goToScreen('biru')}>
                        <Text style={styles.text}>B</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.kuning} onPress={()=> this.goToScreen('kuning')}>
                        <Text style={styles.text}>Y</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}



export default beranda;

const styles = StyleSheet.create({
    container:{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor : 'transparent'
    },
    text:{
        color: 'white',
        fontSize: 70
    },
    layer :{
        flex: 1,
        flexDirection: 'row'
    },
    merah :{
        flex: 1,
        backgroundColor: 'red',
        justifyContent: 'center',
        alignItems: 'center'
    },
    hijau :{
        flex: 1,
        backgroundColor: 'green',
        justifyContent: 'center',
        alignItems: 'center'
    },
    biru :{
        flex: 1,
        backgroundColor: 'blue',
        justifyContent: 'center',
        alignItems: 'center'
    },
    kuning :{
        flex: 1,
        backgroundColor: 'yellow',
        justifyContent: 'center',
        alignItems: 'center'
    }


});