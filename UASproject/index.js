/** @format */

import { Navigation } from "react-native-navigation";

import beranda from './screen/beranda'
import merah from './screen/merah'
import hijau from './screen/hijau'
import biru from './screen/biru'
import kuning from './screen/kuning'


Navigation.registerComponent('beranda',() => beranda);
Navigation.registerComponent('merah',() => merah);
Navigation.registerComponent('hijau',() => hijau);
Navigation.registerComponent('biru',() => biru);
Navigation.registerComponent('kuning',() => kuning);

Navigation.events().registerAppLaunchedListener(() => {
      Navigation.setRoot({
            root: {
              stack: {
                    id: 'AppStack',
                  children:[
                      {
                        component: {
                            name: 'beranda'
                        },
                    },
                    ]
                }
            }
      });
    });